package za.co.enerweb.toolbox.poi.ss;

import static java.lang.String.format;

import java.util.Iterator;

import lombok.Data;

import org.apache.poi.hssf.util.PaneInformation;
import org.apache.poi.ss.usermodel.AutoFilter;
import org.apache.poi.ss.usermodel.CellRange;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.SheetConditionalFormatting;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;

/**
 * A spreadsheet sheet
 */
@Data
public class Sheet implements org.apache.poi.ss.usermodel.Sheet {

    // private interface Excluded {
    // Row getRow(int rownum);
    // }
    // @Delegate(excludes = Excluded.class)
    private org.apache.poi.ss.usermodel.Sheet poiSheet;

    private int maxColumn = -1;
    private int minColumn = Integer.MAX_VALUE;

    private final Spreadsheet spreadsheet;

    /**
     * Use Spreadsheet.addSheet() to create one of these bad mamma jammas
     */
    protected Sheet(Spreadsheet spreadsheet,
        org.apache.poi.ss.usermodel.Sheet poiSheet) {
        this.spreadsheet = spreadsheet;
        this.poiSheet = poiSheet;
    }

    public SheetWalker getSheetWalker() {
        return new SheetWalker(this);
    }

    public Cell getCell(int rownum, int colnum) {
        return getRow(rownum).getCell(colnum);
    }

    public Cell getCell(CellReference cellReference) {
        String refSheetName = cellReference.getSheetName();
        if (refSheetName != null && !refSheetName.equals(getSheetName())) {
            throw new IllegalArgumentException(format(
                "Requesting a cell that is not on this sheet(%s): '%s'",
                getSheetName(), refSheetName
                ));
        }
        return getCell(cellReference.getRow(), cellReference.getCol());
    }

    void updateColumnLimits(int colnum) {
        if (colnum > maxColumn){
            maxColumn = colnum;
        }
        if (colnum < minColumn){
            minColumn = colnum;
        }
    }


    public void setCell(int rownum, int colnum, Number value) {
        getCell(rownum, colnum).setCellValue(value.doubleValue());
    }

    public void setCell(int rownum, int colnum, String value) {
        getCell(rownum, colnum).setCellValue(value);
    }

    /**
     * This will automatically create it if needed.
     * @param rownum row to get (0-based)
     */
    public za.co.enerweb.toolbox.poi.ss.Row getRow(int rownum) {
        Row row = poiSheet.getRow(rownum);
        if (row == null) {
            row = poiSheet.createRow(rownum);
        }
        return new za.co.enerweb.toolbox.poi.ss.Row(this, row);
    }

    public boolean hasRow(int rownum) {
        return poiSheet.getRow(rownum) != null;
    }

    /**
     * When adding cells we keep track of the range of columns,
     * this method will resize every column in that range.
     */
    public void autoSizeColumns() {
        for (int i = minColumn; i <= maxColumn; i++) {
            autoSizeColumn(i);
        }
    }

    public String getName() {
        return poiSheet.getSheetName();
    }

    // vvv delegated methods

    public Iterator<Row> iterator() {
        return poiSheet.iterator();
    }

    public Row createRow(int rownum) {
        return poiSheet.createRow(rownum);
    }

    public void removeRow(Row row) {
        poiSheet.removeRow(row);
    }

    public int getPhysicalNumberOfRows() {
        return poiSheet.getPhysicalNumberOfRows();
    }

    public int getFirstRowNum() {
        return poiSheet.getFirstRowNum();
    }

    public int getLastRowNum() {
        return poiSheet.getLastRowNum();
    }

    public void setColumnHidden(int columnIndex, boolean hidden) {
        poiSheet.setColumnHidden(columnIndex, hidden);
    }

    public boolean isColumnHidden(int columnIndex) {
        return poiSheet.isColumnHidden(columnIndex);
    }

    public void setColumnWidth(int columnIndex, int width) {
        poiSheet.setColumnWidth(columnIndex, width);
    }

    public int getColumnWidth(int columnIndex) {
        return poiSheet.getColumnWidth(columnIndex);
    }

    public void setDefaultColumnWidth(int width) {
        poiSheet.setDefaultColumnWidth(width);
    }

    public int getDefaultColumnWidth() {
        return poiSheet.getDefaultColumnWidth();
    }

    public short getDefaultRowHeight() {
        return poiSheet.getDefaultRowHeight();
    }

    public float getDefaultRowHeightInPoints() {
        return poiSheet.getDefaultRowHeightInPoints();
    }

    public void setDefaultRowHeight(short height) {
        poiSheet.setDefaultRowHeight(height);
    }

    public void setDefaultRowHeightInPoints(float height) {
        poiSheet.setDefaultRowHeightInPoints(height);
    }

    public CellStyle getColumnStyle(int column) {
        return poiSheet.getColumnStyle(column);
    }

    public int addMergedRegion(CellRangeAddress region) {
        return poiSheet.addMergedRegion(region);
    }

    public void setVerticallyCenter(boolean value) {
        poiSheet.setVerticallyCenter(value);
    }

    public void setHorizontallyCenter(boolean value) {
        poiSheet.setHorizontallyCenter(value);
    }

    public boolean getHorizontallyCenter() {
        return poiSheet.getHorizontallyCenter();
    }

    public boolean getVerticallyCenter() {
        return poiSheet.getVerticallyCenter();
    }

    public void removeMergedRegion(int index) {
        poiSheet.removeMergedRegion(index);
    }

    public int getNumMergedRegions() {
        return poiSheet.getNumMergedRegions();
    }

    public CellRangeAddress getMergedRegion(int index) {
        return poiSheet.getMergedRegion(index);
    }

    public Iterator<Row> rowIterator() {
        return poiSheet.rowIterator();
    }

    public void setAutobreaks(boolean value) {
        poiSheet.setAutobreaks(value);
    }

    public void setDisplayGuts(boolean value) {
        poiSheet.setDisplayGuts(value);
    }

    public void setDisplayZeros(boolean value) {
        poiSheet.setDisplayZeros(value);
    }

    public boolean isDisplayZeros() {
        return poiSheet.isDisplayZeros();
    }

    public void setFitToPage(boolean value) {
        poiSheet.setFitToPage(value);
    }

    public void setRowSumsBelow(boolean value) {
        poiSheet.setRowSumsBelow(value);
    }

    public void setRowSumsRight(boolean value) {
        poiSheet.setRowSumsRight(value);
    }

    public boolean getAutobreaks() {
        return poiSheet.getAutobreaks();
    }

    public boolean getDisplayGuts() {
        return poiSheet.getDisplayGuts();
    }

    public boolean getFitToPage() {
        return poiSheet.getFitToPage();
    }

    public boolean getRowSumsBelow() {
        return poiSheet.getRowSumsBelow();
    }

    public boolean getRowSumsRight() {
        return poiSheet.getRowSumsRight();
    }

    public boolean isPrintGridlines() {
        return poiSheet.isPrintGridlines();
    }

    public void setPrintGridlines(boolean show) {
        poiSheet.setPrintGridlines(show);
    }

    public PrintSetup getPrintSetup() {
        return poiSheet.getPrintSetup();
    }

    public Header getHeader() {
        return poiSheet.getHeader();
    }

    public Footer getFooter() {
        return poiSheet.getFooter();
    }

    public void setSelected(boolean value) {
        poiSheet.setSelected(value);
    }

    public double getMargin(short margin) {
        return poiSheet.getMargin(margin);
    }

    public void setMargin(short margin, double size) {
        poiSheet.setMargin(margin, size);
    }

    public boolean getProtect() {
        return poiSheet.getProtect();
    }

    public void protectSheet(String password) {
        poiSheet.protectSheet(password);
    }

    public boolean getScenarioProtect() {
        return poiSheet.getScenarioProtect();
    }

    public void setZoom(int numerator, int denominator) {
        poiSheet.setZoom(numerator, denominator);
    }

    public short getTopRow() {
        return poiSheet.getTopRow();
    }

    public short getLeftCol() {
        return poiSheet.getLeftCol();
    }

    public void showInPane(short toprow, short leftcol) {
        poiSheet.showInPane(toprow, leftcol);
    }

    public void shiftRows(int startRow, int endRow, int n) {
        poiSheet.shiftRows(startRow, endRow, n);
    }

    public void shiftRows(int startRow, int endRow, int n,
        boolean copyRowHeight, boolean resetOriginalRowHeight) {
        poiSheet.shiftRows(startRow, endRow, n, copyRowHeight,
            resetOriginalRowHeight);
    }

    public void createFreezePane(int colSplit, int rowSplit,
        int leftmostColumn, int topRow) {
        poiSheet.createFreezePane(colSplit, rowSplit, leftmostColumn, topRow);
    }

    public void createFreezePane(int colSplit, int rowSplit) {
        poiSheet.createFreezePane(colSplit, rowSplit);
    }

    public void createSplitPane(int xSplitPos, int ySplitPos,
        int leftmostColumn, int topRow, int activePane) {
        poiSheet.createSplitPane(xSplitPos, ySplitPos, leftmostColumn, topRow,
            activePane);
    }

    public PaneInformation getPaneInformation() {
        return poiSheet.getPaneInformation();
    }

    public void setDisplayGridlines(boolean show) {
        poiSheet.setDisplayGridlines(show);
    }

    public boolean isDisplayGridlines() {
        return poiSheet.isDisplayGridlines();
    }

    public void setDisplayFormulas(boolean show) {
        poiSheet.setDisplayFormulas(show);
    }

    public boolean isDisplayFormulas() {
        return poiSheet.isDisplayFormulas();
    }

    public void setDisplayRowColHeadings(boolean show) {
        poiSheet.setDisplayRowColHeadings(show);
    }

    public boolean isDisplayRowColHeadings() {
        return poiSheet.isDisplayRowColHeadings();
    }

    public void setRowBreak(int row) {
        poiSheet.setRowBreak(row);
    }

    public boolean isRowBroken(int row) {
        return poiSheet.isRowBroken(row);
    }

    public void removeRowBreak(int row) {
        poiSheet.removeRowBreak(row);
    }

    public int[] getRowBreaks() {
        return poiSheet.getRowBreaks();
    }

    public int[] getColumnBreaks() {
        return poiSheet.getColumnBreaks();
    }

    public void setColumnBreak(int column) {
        poiSheet.setColumnBreak(column);
    }

    public boolean isColumnBroken(int column) {
        return poiSheet.isColumnBroken(column);
    }

    public void removeColumnBreak(int column) {
        poiSheet.removeColumnBreak(column);
    }

    public void setColumnGroupCollapsed(int columnNumber, boolean collapsed) {
        poiSheet.setColumnGroupCollapsed(columnNumber, collapsed);
    }

    public void groupColumn(int fromColumn, int toColumn) {
        poiSheet.groupColumn(fromColumn, toColumn);
    }

    public void ungroupColumn(int fromColumn, int toColumn) {
        poiSheet.ungroupColumn(fromColumn, toColumn);
    }

    public void groupRow(int fromRow, int toRow) {
        poiSheet.groupRow(fromRow, toRow);
    }

    public void ungroupRow(int fromRow, int toRow) {
        poiSheet.ungroupRow(fromRow, toRow);
    }

    public void setRowGroupCollapsed(int row, boolean collapse) {
        poiSheet.setRowGroupCollapsed(row, collapse);
    }

    public void setDefaultColumnStyle(int column, CellStyle style) {
        poiSheet.setDefaultColumnStyle(column, style);
    }

    public void autoSizeColumn(int column) {
        poiSheet.autoSizeColumn(column);
    }

    public void autoSizeColumn(int column, boolean useMergedCells) {
        poiSheet.autoSizeColumn(column, useMergedCells);
    }

    public Comment getCellComment(int row, int column) {
        return poiSheet.getCellComment(row, column);
    }

    public Drawing createDrawingPatriarch() {
        return poiSheet.createDrawingPatriarch();
    }

    public Workbook getWorkbook() {
        return poiSheet.getWorkbook();
    }

    public String getSheetName() {
        return poiSheet.getSheetName();
    }

    public boolean isSelected() {
        return poiSheet.isSelected();
    }

    public CellRange<? extends org.apache.poi.ss.usermodel.Cell>
            setArrayFormula(String formula,
        CellRangeAddress range) {
        return poiSheet.setArrayFormula(formula, range);
    }

    public CellRange<? extends org.apache.poi.ss.usermodel.Cell>
        removeArrayFormula(org.apache.poi.ss.usermodel.Cell cell) {
        return poiSheet.removeArrayFormula(cell);
    }

    public DataValidationHelper getDataValidationHelper() {
        return poiSheet.getDataValidationHelper();
    }

    public void addValidationData(DataValidation dataValidation) {
        poiSheet.addValidationData(dataValidation);
    }

    public AutoFilter setAutoFilter(CellRangeAddress range) {
        return poiSheet.setAutoFilter(range);
    }

    public boolean getForceFormulaRecalculation() {
        return poiSheet.getForceFormulaRecalculation();
    }

    public void setForceFormulaRecalculation(boolean arg0) {
        poiSheet.setForceFormulaRecalculation(arg0);
    }

    public void setRightToLeft(boolean value) {
        poiSheet.setRightToLeft(value);
    }

    public boolean isRightToLeft() {
        return poiSheet.isRightToLeft();
    }

    public SheetConditionalFormatting getSheetConditionalFormatting() {
        return poiSheet.getSheetConditionalFormatting();
    }

    @Override
    public CellRangeAddress getRepeatingColumns() {
        return poiSheet.getRepeatingColumns();
    }

    @Override
    public CellRangeAddress getRepeatingRows() {
        return poiSheet.getRepeatingRows();
    }

    @Override
    public void setRepeatingColumns(CellRangeAddress arg0) {
        poiSheet.setRepeatingColumns(arg0);
    }

    @Override
    public void setRepeatingRows(CellRangeAddress arg0) {
        poiSheet.setRepeatingRows(arg0);
    }
}
