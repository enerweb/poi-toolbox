package za.co.enerweb.toolbox.poi.ss;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lombok.Data;
import lombok.val;

import org.apache.poi.ss.util.CellReference;

/**
 * This makes it really easy to populate or read a sheet
 * using mostly relative manoeuvres.
 */
@Data
public class SheetWalker {
    private Sheet sheet;
    private int currentRow = 0;
    private int currentColumn = 0;
    private int markedRow = 0;
    private int markedColumn = 0;

    protected SheetWalker(Sheet sheet) {
        this.sheet = sheet;
    }

    /**
     * Does not change the current cell
     */
    public Cell getCell(int rownum, int colnum) {
        return sheet.getCell(rownum, colnum);
    }

    public Cell getCell(CellReference cellReference) {
        return sheet.getCell(cellReference);
    }

    public Cell getCell(String cellReference) {
        return sheet.getCell(new CellReference(cellReference));
    }

    /**
     * Gets a cell that is relative to the current one,
     * without changing the current position.
     * @param relativeRownum may be negative.
     */
    public Cell getRelativeCell(int relativeRownum, int relativeColnum) {
        return sheet.getCell(currentRow + relativeRownum,
            currentColumn + relativeColnum);
    }

    /**
     * Updates the current cell, to this.
     */
    public Cell moveTo(int rownum, int colnum) {
        return sheet.getCell(currentRow = rownum, currentColumn = colnum);
    }

    public Cell current() {
        return sheet.getCell(currentRow, currentColumn);
    }

    /**
     * cell to the right of the current cell
     */
    public Cell nextColumn() {
        return nextColumn(0);
    }

    /**
     * cell to the right of the current cell (skipping over a couple)
     */
    public Cell nextColumn(int skipColumns) {
        return nextColumn(0, skipColumns);
    }

    /**
     * cell to the right of the current cell (skipping over a couple)
     */
    public Cell nextColumn(int skipRows, int skipColumns) {
        return sheet.getCell(currentRow += skipRows,
            currentColumn += skipColumns + 1);
    }

    /**
     * Go to the first row of the next column to the right.
     */
    public Cell nextColumnTop() {
        return nextColumnTop(0, 0);
    }

    /**
     * Go to the first row of the next column to the right
     * (skipping over a couple of columns)
     */
    public Cell nextColumnTop(int skipColumns) {
        return nextColumnTop(0, skipColumns);
    }

    /**
     * Go to the first row of the next column to the right
     * (skipping over a couple of columns)
     */
    public Cell nextColumnTop(int skipRows, int skipColumns) {
        return sheet.getCell(currentRow = skipRows,
            currentColumn += skipColumns + 1);
    }

    /**
     * sets row to -1 in preperation for a nextRow call
     */
    public void setAboveTop() {
        setAboveTop(0);
    }

    /**
     * sets row to -1 in preperation for a nextRow call
     */
    public void setAboveTop(int skipColumns) {
        currentRow = -1;
        currentColumn += skipColumns;
    }

    /**
     * @param skipColumns may be negative
     */
    public Cell moveHorizontally(int skipColumns) {
        currentColumn += skipColumns;
        return current();
    }

    /**
     * go to the cell of the first column of the next row
     */
    public Cell nextRow() {
        return nextRow(0);
    }

    /**
     * go to the cell of the first column of the next row
     * (skipping over a couple of rows)
     */
    public Cell nextRow(int skipRows) {
        return nextRow(skipRows, 0);
    }

    /**
     * go to the cell of the first column of the next row
     * (skipping over a couple of rows and columns)
     */
    public Cell nextRow(int skipRows, int skipColumns) {
        return sheet.getCell(currentRow += skipRows + 1,
            currentColumn += skipColumns);
    }

    /**
     * go to the cell of the first column of the next row
     */
    public Cell nextRowLeft() {
        return nextRowLeft(0);
    }

    /**
     * go to the cell of the first column of the next row
     * (skipping over a couple of rows)
     */
    public Cell nextRowLeft(int skipRows) {
        return nextRowLeft(skipRows, 0);
    }

    /**
     * go to the cell of the first column of the next row
     * (skipping over a couple of rows and columns)
     */
    public Cell nextRowLeft(int skipRows, int skipColumns) {
        return sheet.getCell(currentRow += skipRows + 1,
            currentColumn = skipColumns);
    }

    /**
     * sets column to -1 in preperation for a nextColumn call
     */
    public void setBeforeLeft() {
        setBeforeLeft(0);
    }

    /**
     * sets column to -1 in preperation for a nextColumn call
     */
    public void setBeforeLeft(int skipRows) {
        currentRow += skipRows;
        currentColumn = -1;
    }

    /**
     * @param skipRows may be negative
     * @return
     */
    public Cell moveVertically(int skipRows) {
        currentRow += skipRows;
        return current();
    }

    // vvv mark and gotoMark api
    public void mark() {
        markedRow = currentRow;
        markedColumn = currentColumn;
    }

    public Cell moveToMark() {
        return moveTo(markedRow, markedColumn);
    }

    /**
     * start at the current position and set successive columns to the
     * specified values
     * @param values
     */
    public void setRow(Object... values) {
        setRow(asList(values));
    }

    public void setRow(Collection<?> values) {
        if (values.size() == 0) {
            return;
        }
        currentColumn--;
        for (Object value : values) {
            nextColumn().value(value);
        }
    }

    /**
     * Reads values until it finds an empty cell
     * @return
     */
    public <T> List<T> getRow() {
        val ret = new ArrayList<T>();
        currentColumn--;
        while (true) {
            T curVal = nextColumn().getValue();
            if (curVal == null) {
                break;
            }
            ret.add(curVal);
        }
        return ret;
    }
}
