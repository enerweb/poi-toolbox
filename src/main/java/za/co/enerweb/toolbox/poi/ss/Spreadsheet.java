package za.co.enerweb.toolbox.poi.ss;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;

import lombok.Cleanup;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * This is a Spreadsheet facade over the poi library, to make it easier to
 * work with spreadsheets.
 *
 * Not delegating the workbook, because we are trying to hide most of it.
 * But when you do need advanced features use getWorkbook().
 */
@Slf4j
public class Spreadsheet {
    private static final String EXTENTION_XLSX = "xlsx";
    private static final String EXTENTION_XLSM = "xlsm";
    private static final String EXTENTION_XLS = "xls";
    private File file;
    private String fileExtention;
    private Workbook wb;
    @Setter
    private OutputStream outputStream;
    @Setter
    private InputStream inputStream;
    private FormulaEvaluator formulaEvaluator;

    /**
     * Just know that you will not be able so save or load unless
     * Rather using static constructors if they suit you.
     */
    public Spreadsheet() {
    }

    /*
     * We could add stream based constuctors later,
     * they will at least need to give a filename (or even mimetype)
     * so we can detect the format
     */
    public static Spreadsheet forFile(File file) {
        Spreadsheet spreadsheet = new Spreadsheet();
        spreadsheet.setFile(file);
        return spreadsheet;
    }

    /**
     * @param filename we are mostly interested in the extension to detect the
     *        file type
     * @param outputStream
     * @return
     */
    public static Spreadsheet forStream(String filename,
            OutputStream outputStream) {
        Spreadsheet spreadsheet = new Spreadsheet();
        spreadsheet.setFileName(filename);
        spreadsheet.outputStream = outputStream;
        spreadsheet.initWorkbookForWriting();
        return spreadsheet;
    }

    public static Spreadsheet forStream(String filename,
        InputStream inputStream) {
        Spreadsheet spreadsheet = new Spreadsheet();
        spreadsheet.setFileName(filename);
        spreadsheet.inputStream = inputStream;
        return spreadsheet;
    }

    public void setFile(File file) {
        this.file = file;
        setFileName(file.getName());
        initWorkbookForWriting();
    }

    public void setFileName(String fileName) {
        setFileExtention(FilenameUtils.getExtension(fileName));
    }

    public void setFileExtention(String fileExtention) {
        this.fileExtention = fileExtention;
    }

    /**
     * creates a new workbook based on the file current file extension
     */
    public void initWorkbookForWriting() {
        formulaEvaluator = null;
        if (fileExtention == null ){
            throw new IllegalArgumentException("No file extension was "
                + "specified, so the desired file format can not be detected.");
        } else if (fileExtention.equals(EXTENTION_XLS)) {
            wb = new HSSFWorkbook();
        } else if (fileExtention.equals(EXTENTION_XLSX)
            || fileExtention.equals(EXTENTION_XLSM)) {
            wb = new XSSFWorkbook();
        } else {
            throw new IllegalArgumentException("Files of type " + fileExtention
                + " are not supported.\n"
                + "Only the following types are supported: "
                + EXTENTION_XLS + ", " + EXTENTION_XLSX + ", "
                + EXTENTION_XLSM);
        }
    }

    /*
     * maybe we should aggregate all the errors and throw an exception..
     */
    public void evaluateAllFormulaCells() {
        assumeWorkbook();
        // if (wb instanceof HSSFWorkbook) {
        // HSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
        // } else if (wb instanceof XSSFWorkbook) {
        // XSSFFormulaEvaluator.evaluateAllFormulaCells((XSSFWorkbook) wb);
        // }

        getFormulaEvaluator();
        log.debug("before evaluate");
        for (int sheetNum = 0; sheetNum < wb.getNumberOfSheets(); sheetNum++) {
            org.apache.poi.ss.usermodel.Sheet sheet = wb.getSheetAt(sheetNum);
            for (org.apache.poi.ss.usermodel.Row r : sheet) {
                for (org.apache.poi.ss.usermodel.Cell c : r) {
                    if (c.getCellType() == Cell.CELL_TYPE_FORMULA) {
                        try {
                            formulaEvaluator.evaluateFormulaCell(c);
                        } catch (RuntimeException e) {
                            log.warn(
                                "Could not evaluate cell: "
                                    + new CellReference(c).formatAsString()
                                    + " ("
                                    + sheet.getSheetName() + "!"
                                    + r.getRowNum() + ","
                                    + c.getColumnIndex()
                                    + "): "
                                    + c.getCellFormula() + " "
                                    + e.getMessage());
                        }
                    }
                }
            }
        }
        log.debug("after evaluate");
    }

    private FormulaEvaluator getFormulaEvaluator() {
        if (formulaEvaluator == null) {
            formulaEvaluator = wb
                .getCreationHelper().createFormulaEvaluator();
        }
        return formulaEvaluator;
    }

    void notifyUpdateCell(Cell cell) {
        if (formulaEvaluator != null) {
            getFormulaEvaluator().notifyUpdateCell(cell.getPoiCell());
        }
    }

    public Workbook getWorkbook() {
        return assumeWorkbook();
    }

    private OutputStream getOutputStream() throws IOException {
        if (outputStream != null) {
            return outputStream;
        }
        if (file != null) {
            return new FileOutputStream(file);
        }
        throw new IllegalStateException("Could not get output stream, "
            + "because no file or outputstream were specified.");
    }

    public void save() throws IOException {
        @Cleanup OutputStream out = getOutputStream();
        save(out);
    }

    public void save(OutputStream out) throws IOException {
        assumeWorkbook().write(out);
    }

    private InputStream getInputStream() throws IOException {
        if (inputStream != null) {
            return inputStream;
        }
        if (file != null) {
            return new FileInputStream(file);
        }
        throw new IllegalStateException("Could not get input stream, "
            + "because no file or inputstream were specified.");
    }

    public Spreadsheet load() throws InvalidFormatException, IOException {
        return load(getInputStream());
    }

    public Spreadsheet load(InputStream inputStream)
        throws InvalidFormatException, IOException {
        wb = WorkbookFactory.create(inputStream);
        formulaEvaluator = null;
        return this;
    }

    /**
     * Get sheet with the given name,
     * if it does not exist it will be created.
     * Invalid characters are converted to spaces.
     * @param name
     * @return
     */
    public Sheet getSheet(String name) {
        String safeName = getSafeSheetName(name);
        org.apache.poi.ss.usermodel.Sheet sheet = assumeWorkbook()
            .getSheet(safeName);
        if (sheet == null) {
            sheet = assumeWorkbook().createSheet(safeName);
        }
        return new Sheet(this, sheet);
    }

    public Sheet getSheet(int index) {
        org.apache.poi.ss.usermodel.Sheet sheet = assumeWorkbook()
            .getSheetAt(index);
        return new Sheet(this, sheet);
    }

    public String getSafeSheetName(String name) {
        return WorkbookUtil.createSafeSheetName(name);
    }

    /**
     * Does not create a sheet
     */
    public Sheet getSheetAt(int index) {
        return new Sheet(this, assumeWorkbook().getSheetAt(index));
    }

    public int getSheetCount() {
        return assumeWorkbook().getNumberOfSheets();
    }

    public boolean hasSheet(String name) {
        String safeName = getSafeSheetName(name);
        org.apache.poi.ss.usermodel.Sheet sheet = assumeWorkbook()
            .getSheet(safeName);
        return sheet != null;
    }

    /**
     * Get/Create sheet and give you a cool walker..
     */
    public SheetWalker getSheetWalker(String sheetName) {
        return getSheet(sheetName).getSheetWalker();
    }

    public SheetWalker getSheetWalker(int sheetIndex) {
        return getSheet(sheetIndex).getSheetWalker();
    }

    // vvv create some styles
    // we have some nice methods here to get you on your way,
    // but when you have it you can tweek it a little :)
    public CellStyle createCellStyle() {
        return new CellStyle(this);
    }

    public CellStyle createHeaderCellStyle() {
        return createHeaderCellStyle(10);
    }

    public CellStyle createHeaderCellStyle(int fontHeight) {
        return createCellStyle().text().bold()
            .backgroundColor(IndexedColors.GREY_25_PERCENT)
            .fontHeight(fontHeight);
    }

    public Collection<Sheet> sheets() {
        int sheetCount = assumeWorkbook().getNumberOfSheets();
        ArrayList<Sheet> ret = new ArrayList<Sheet>(sheetCount);
        for (int i = 0; i < sheetCount; i++) {
            ret.add(new Sheet(this, assumeWorkbook().getSheetAt(i)));
        }
        return ret;
    }

    private Workbook assumeWorkbook() {
        if (wb == null) {
            throw new IllegalStateException(
                "No workbook has been initialised. Either call load() " +
                    "or initWorkbookForWriting().");
        }
        return wb;
    }

    public void autoSizeColumns() {
        for (Sheet sheet : sheets()) {
            sheet.autoSizeColumns();
        }
    }

}
