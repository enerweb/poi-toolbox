package za.co.enerweb.toolbox.poi.ss;

import lombok.Getter;

import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;

public class CellStyle implements org.apache.poi.ss.usermodel.CellStyle {
    @SuppressWarnings("unused")
    private Spreadsheet spreadsheet;
    private Workbook wb;
    @Getter
    private org.apache.poi.ss.usermodel.CellStyle poiCellStyle;
    private Font font;

    /**
     * Use Spreadsheet.createCellStyle() to create one of these bad mamma jammas
     */
    protected CellStyle(Spreadsheet spreadsheet) {
        this.spreadsheet = spreadsheet;
        wb = spreadsheet.getWorkbook();
        poiCellStyle = wb.createCellStyle();
        font = wb.createFont();
        poiCellStyle.setFont(font);
    }

    // vvv fluent api's to nicely set up your style,

    public CellStyle fontHeight(int points) {
        font.setFontHeightInPoints((short) points);
        return this;
    }

    public CellStyle bold() {
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        return this;
    }

    public CellStyle text() {
        poiCellStyle.setDataFormat(wb.createDataFormat().getFormat("text"));
        return this;
    }

    /**
     * number(0.###)
     */
    public CellStyle number() {
        return number("0.###");
    }

    /**
     * @param numberFormat eg. "0.###" or "0.000"
     * or dates eg. YYYY-MM-DD or (YYYY/MM/DD HH:MM:SS
     */
    public CellStyle number(String numberFormat) {
        poiCellStyle.setDataFormat(wb.createDataFormat().getFormat(
            numberFormat));
        return this;
    }

    /**
     * number(YYYY-MM-DD)
     */
    public CellStyle date() {
        return number("YYYY-MM-DD");
    }

    /**
     * number(YYYY-MM-DD HH:MM:SS)
     */
    public CellStyle dateTime() {
        return number("YYYY-MM-DD HH:MM:SS");
    }

    /**
     * Sets the background colour for the cell
     */
    public CellStyle backgroundColor(IndexedColors indexedColor) {
        // trust me this works, the poi api is a bit confusing..

        // set the foreground of the fill style
        poiCellStyle.setFillForegroundColor(indexedColor.getIndex());
        // fill the whole thing with the foreground
        poiCellStyle.setFillPattern(
            org.apache.poi.ss.usermodel.CellStyle.SOLID_FOREGROUND);
        // since we are using a boring monotone fill we dont need to set the
        // background colour of the fill
        return this;
    }

    // vvv lotsa border helper mehtods

    /**
     * set the left border
     */
    public CellStyle left(Border borderStyle) {
        poiCellStyle.setBorderLeft(borderStyle.getPoiType());
        return this;
    }

    /**
     * set the right border
     */
    public CellStyle rigth(Border borderStyle) {
        poiCellStyle.setBorderRight(borderStyle.getPoiType());
        return this;
    }

    /**
     * set the top border
     */
    public CellStyle top(Border borderStyle) {
        poiCellStyle.setBorderTop(borderStyle.getPoiType());
        return this;
    }

    /**
     * set the bottom border
     */
    public CellStyle bottom(Border borderStyle) {
        poiCellStyle.setBorderBottom(borderStyle.getPoiType());
        return this;
    }

    /**
     * Sets the border right around, in order to only set one of the sides
     * use the left(), right(), top(), bottom() methods
     * @param borderStyle
     * @return
     */
    public CellStyle border(Border borderStyle) {
        return left(borderStyle).rigth(borderStyle).top(borderStyle).bottom(
            borderStyle);
    }

    /**
     * Thin border right around
     */
    // only here because it can possibly be used a lot!
    public CellStyle thinBorder() {
        return border(Border.THIN);
    }

    // vvv delegated methods

    public short getIndex() {
        return poiCellStyle.getIndex();
    }

    public void setDataFormat(short fmt) {
        poiCellStyle.setDataFormat(fmt);
    }

    public short getDataFormat() {
        return poiCellStyle.getDataFormat();
    }

    public String getDataFormatString() {
        return poiCellStyle.getDataFormatString();
    }

    public void setFont(Font font) {
        poiCellStyle.setFont(font);
    }

    public short getFontIndex() {
        return poiCellStyle.getFontIndex();
    }

    public void setHidden(boolean hidden) {
        poiCellStyle.setHidden(hidden);
    }

    public boolean getHidden() {
        return poiCellStyle.getHidden();
    }

    public void setLocked(boolean locked) {
        poiCellStyle.setLocked(locked);
    }

    public boolean getLocked() {
        return poiCellStyle.getLocked();
    }

    public void setAlignment(short align) {
        poiCellStyle.setAlignment(align);
    }

    public short getAlignment() {
        return poiCellStyle.getAlignment();
    }

    public void setWrapText(boolean wrapped) {
        poiCellStyle.setWrapText(wrapped);
    }

    public boolean getWrapText() {
        return poiCellStyle.getWrapText();
    }

    public void setVerticalAlignment(short align) {
        poiCellStyle.setVerticalAlignment(align);
    }

    public short getVerticalAlignment() {
        return poiCellStyle.getVerticalAlignment();
    }

    public void setRotation(short rotation) {
        poiCellStyle.setRotation(rotation);
    }

    public short getRotation() {
        return poiCellStyle.getRotation();
    }

    public void setIndention(short indent) {
        poiCellStyle.setIndention(indent);
    }

    public short getIndention() {
        return poiCellStyle.getIndention();
    }

    public void setBorderLeft(short border) {
        poiCellStyle.setBorderLeft(border);
    }

    public short getBorderLeft() {
        return poiCellStyle.getBorderLeft();
    }

    public void setBorderRight(short border) {
        poiCellStyle.setBorderRight(border);
    }

    public short getBorderRight() {
        return poiCellStyle.getBorderRight();
    }

    public void setBorderTop(short border) {
        poiCellStyle.setBorderTop(border);
    }

    public short getBorderTop() {
        return poiCellStyle.getBorderTop();
    }

    public void setBorderBottom(short border) {
        poiCellStyle.setBorderBottom(border);
    }

    public short getBorderBottom() {
        return poiCellStyle.getBorderBottom();
    }

    public void setLeftBorderColor(short color) {
        poiCellStyle.setLeftBorderColor(color);
    }

    public short getLeftBorderColor() {
        return poiCellStyle.getLeftBorderColor();
    }

    public void setRightBorderColor(short color) {
        poiCellStyle.setRightBorderColor(color);
    }

    public short getRightBorderColor() {
        return poiCellStyle.getRightBorderColor();
    }

    public void setTopBorderColor(short color) {
        poiCellStyle.setTopBorderColor(color);
    }

    public short getTopBorderColor() {
        return poiCellStyle.getTopBorderColor();
    }

    public void setBottomBorderColor(short color) {
        poiCellStyle.setBottomBorderColor(color);
    }

    public short getBottomBorderColor() {
        return poiCellStyle.getBottomBorderColor();
    }

    public void setFillPattern(short fp) {
        poiCellStyle.setFillPattern(fp);
    }

    public short getFillPattern() {
        return poiCellStyle.getFillPattern();
    }

    public void setFillBackgroundColor(short bg) {
        poiCellStyle.setFillBackgroundColor(bg);
    }

    public short getFillBackgroundColor() {
        return poiCellStyle.getFillBackgroundColor();
    }

    public Color getFillBackgroundColorColor() {
        return poiCellStyle.getFillBackgroundColorColor();
    }

    public void setFillForegroundColor(short bg) {
        poiCellStyle.setFillForegroundColor(bg);
    }

    public short getFillForegroundColor() {
        return poiCellStyle.getFillForegroundColor();
    }

    public Color getFillForegroundColorColor() {
        return poiCellStyle.getFillForegroundColorColor();
    }

    public void cloneStyleFrom(org.apache.poi.ss.usermodel.CellStyle source) {
        poiCellStyle.cloneStyleFrom(source);
    }
}
