package za.co.enerweb.toolbox.poi.ss;

import java.util.Calendar;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;

import org.apache.poi.ss.formula.FormulaParseException;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

@Slf4j
public class Cell implements org.apache.poi.ss.usermodel.Cell {
    private org.apache.poi.ss.usermodel.Cell poiCell;
    private final za.co.enerweb.toolbox.poi.ss.Row row;

    public Cell(za.co.enerweb.toolbox.poi.ss.Row row,
        org.apache.poi.ss.usermodel.Cell poiCell) {
        this.row = row;
        this.poiCell = poiCell;
    }

    // vvv more fluent api for setting stuff

    public Cell value(boolean value) {
        poiCell.setCellType(CELL_TYPE_BOOLEAN);
        poiCell.setCellValue(value);
        notifyUpdateCell();
        return this;
    }

    public Cell value(Boolean value) {
        return value(value.booleanValue());
    }

    public Cell value(Number value) {
        return value(value.doubleValue());
    }

    public Cell value(double value) {
        poiCell.setCellType(CELL_TYPE_NUMERIC);
        poiCell.setCellValue(value);
        notifyUpdateCell();
        return this;
    }

    private void notifyUpdateCell() {
        row.getTbSheet().getSpreadsheet().notifyUpdateCell(this);
    }

    public Cell value(Date value) {
        poiCell.setCellType(CELL_TYPE_NUMERIC);
        poiCell.setCellValue(value);
        notifyUpdateCell();
        return this;
    }

    public Cell value(Calendar value) {
        return value(value.getTime());
    }

    public Cell value(RichTextString value) {
        poiCell.setCellValue(value);
        return this;
    }

    public Cell value(String value) {
        poiCell.setCellType(CELL_TYPE_STRING);
        poiCell.setCellValue(value);
        notifyUpdateCell();
        return this;
    }

    /**
     * Smart value setter, tries to detect the cell type.
     * @param NB. it can only support String, Number and Boolean inputs.
     */
    public Cell value(Object value) {
        if (value == null) {
            setCellType(CELL_TYPE_BLANK);
            notifyUpdateCell();
            return this;
        }
        if (value instanceof Boolean) {
            return value((Boolean) value);
        }
        if (value instanceof Number) {
            return value((Number) value);
        }
        if (value instanceof String) {
            return value((String) value);
        }
        throw new IllegalArgumentException("Could not detect the value of "
            + "type: " + value.getClass());
    }

    /**
     * If you want to set a value, do that before you set the formula.
     */
    public Cell formula(String formula) throws FormulaParseException {
        poiCell.setCellFormula(formula);
        notifyUpdateCell();
        return this;
    }

    public Cell style(CellStyle style) {
        poiCell.setCellStyle(style);
        return this;
    }

    public Cell style(za.co.enerweb.toolbox.poi.ss.CellStyle style) {
        poiCell.setCellStyle(style.getPoiCellStyle());
        return this;
    }

    /**
     * Smart value getter, tries to detect the cell type.
     * NB. This can't really support values from formulas :(
     * @return NB. it can only support String, Double and Boolean outputs.
     *         It will also spit out Formulas numeric values!
     */
    public <T> T getValue() {
        return getValue(poiCell.getCellType());
    }

    @SuppressWarnings("unchecked")
    private <T> T getValue(int type) {
        switch (type) {
        case Cell.CELL_TYPE_FORMULA:
            return getValue(poiCell.getCachedFormulaResultType());
        case Cell.CELL_TYPE_NUMERIC:
            return (T) new Double(poiCell.getNumericCellValue());
        case Cell.CELL_TYPE_STRING:
            return (T) poiCell.getStringCellValue();
        case Cell.CELL_TYPE_BOOLEAN:
            return (T) new Boolean(poiCell.getBooleanCellValue());
        case Cell.CELL_TYPE_BLANK:
            return (T) null;
        case Cell.CELL_TYPE_ERROR:
            throw new IllegalStateException(
                ErrorEval.getText(poiCell.getErrorCellValue()));
        }
        throw new IllegalStateException("Could not detect the cell type: " +
            poiCell.getCellType());
    }

    /**
     * Smart value getter, tries to get the expected value as
     * the specified type.
     * @param only supports String, Number, Integer, Double and Boolean.
     */
    @SuppressWarnings("unchecked")
    public <T> T getValue(Class<T> expectedType) {
        Object ret = getValue();
        if (ret != null) {
            if (!expectedType.isInstance(ret)) {
                if (ret instanceof Double) {
                    if (expectedType.isAssignableFrom(Integer.class)) {
                        return (T) new Integer((int) Math.round((Double) ret));
                    }
                    if (expectedType.isAssignableFrom(Long.class)) {
                        return (T) new Long((long) Math.round((Double) ret));
                    }
                }
                throw new IllegalStateException(
                    "Value " + ret + " is not of expected type: "
                        + expectedType);
            }
        }
        return (T) ret;
    }

    /**
     * for debugging
     */
    public String toString() {
        return toString(poiCell.getCellType());
    }

    private String toString(int type) {
        try {
            switch (type) {
            case Cell.CELL_TYPE_BLANK:
                return "";
            case Cell.CELL_TYPE_ERROR:
                return ErrorEval.getText(poiCell.getErrorCellValue());
            case Cell.CELL_TYPE_FORMULA:
                return poiCell.getCellFormula() + " = " +
                    toString(poiCell.getCachedFormulaResultType());
            case Cell.CELL_TYPE_STRING:
                return poiCell.getStringCellValue();
            case Cell.CELL_TYPE_NUMERIC:
                return new Double(poiCell.getNumericCellValue()).toString();
            case Cell.CELL_TYPE_BOOLEAN:
                return new Boolean(poiCell.getBooleanCellValue()).toString();
            }
            return "Could not detect the cell type: " +
                poiCell.getCellType();
        } catch (RuntimeException e) {
            log.debug("Could not convert cell to text", e);
            return e.getMessage();
        }
    }

    public Date getDateValue() {
        return poiCell.getDateCellValue();
    }

    public String getStringValue() {
        return getValue(String.class);
    }

    public double getDoubleValue() {
        return getValue(Double.class);
    }

    public int getIntValue() {
        return getValue(Integer.class);
    }

    public long getLongValue() {
        return getValue(Long.class);
    }

    // vvv delegated methods

    public int getColumnIndex() {
        return poiCell.getColumnIndex();
    }

    public int getRowIndex() {
        return poiCell.getRowIndex();
    }

    public Sheet getSheet() {
        return poiCell.getSheet();
    }

    public Row getRow() {
        return poiCell.getRow();
    }

    public void setCellType(int cellType) {
        poiCell.setCellType(cellType);
    }

    public int getCellType() {
        return poiCell.getCellType();
    }

    public int getCachedFormulaResultType() {
        return poiCell.getCachedFormulaResultType();
    }

    public void setCellValue(double value) {
        try {
            Double oldValue = getValue(Double.class);
            if (oldValue != null && oldValue == value) {
                return;
            }
        } catch (Exception e) {
            // maybe it is not numeric at the moment
            log.debug("Could not check numeric value when setting to " + value,
                e);
        }
        poiCell.setCellValue(value);
        notifyUpdateCell();
    }

    public void setCellValue(Date value) {
        poiCell.setCellValue(value);
        notifyUpdateCell();
    }

    public void setCellValue(Calendar value) {
        poiCell.setCellValue(value);
        notifyUpdateCell();
    }

    public void setCellValue(RichTextString value) {
        poiCell.setCellValue(value);
        notifyUpdateCell();
    }

    public void setCellValue(String value) {
        poiCell.setCellValue(value);
        notifyUpdateCell();
    }

    public void setCellFormula(String formula) throws FormulaParseException {
        poiCell.setCellFormula(formula);
        notifyUpdateCell();
    }

    public String getCellFormula() {
        return poiCell.getCellFormula();
    }

    public double getNumericCellValue() {
        return poiCell.getNumericCellValue();
    }

    public Date getDateCellValue() {
        return poiCell.getDateCellValue();
    }

    public RichTextString getRichStringCellValue() {
        return poiCell.getRichStringCellValue();
    }

    public String getStringCellValue() {
        return poiCell.getStringCellValue();
    }

    public void setCellValue(boolean value) {
        poiCell.setCellValue(value);
    }

    public void setCellErrorValue(byte value) {
        poiCell.setCellErrorValue(value);
    }

    public boolean getBooleanCellValue() {
        return poiCell.getBooleanCellValue();
    }

    public byte getErrorCellValue() {
        return poiCell.getErrorCellValue();
    }

    public void setCellStyle(CellStyle style) {
        poiCell.setCellStyle(style);
    }

    public void setCellStyle(za.co.enerweb.toolbox.poi.ss.CellStyle style) {
        poiCell.setCellStyle(style.getPoiCellStyle());
    }

    public CellStyle getCellStyle() {
        return poiCell.getCellStyle();
    }

    public void setAsActiveCell() {
        poiCell.setAsActiveCell();
    }

    public void setCellComment(Comment comment) {
        poiCell.setCellComment(comment);
    }

    public Comment getCellComment() {
        return poiCell.getCellComment();
    }

    public void removeCellComment() {
        poiCell.removeCellComment();
    }

    public Hyperlink getHyperlink() {
        return poiCell.getHyperlink();
    }

    public void setHyperlink(Hyperlink link) {
        poiCell.setHyperlink(link);
    }

    public CellRangeAddress getArrayFormulaRange() {
        return poiCell.getArrayFormulaRange();
    }

    public boolean isPartOfArrayFormulaGroup() {
        return poiCell.isPartOfArrayFormulaGroup();
    }

    org.apache.poi.ss.usermodel.Cell getPoiCell() {
        return poiCell;
    }
}
