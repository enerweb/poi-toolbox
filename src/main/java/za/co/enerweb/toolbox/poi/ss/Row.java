package za.co.enerweb.toolbox.poi.ss;

import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;

public class Row implements org.apache.poi.ss.usermodel.Row {
    // private interface Excluded {
    // Cell getCell(int cellnum);
    // }
    // @Delegate(excludes = Excluded.class)
    private org.apache.poi.ss.usermodel.Row poiRow;
    private za.co.enerweb.toolbox.poi.ss.Sheet sheet;

    public Row(za.co.enerweb.toolbox.poi.ss.Sheet sheet,
            org.apache.poi.ss.usermodel.Row poiRow) {
        this.sheet = sheet;
        this.poiRow = poiRow;
    }

    /**
     * This will automatically create it if needed.
     * @param cellnum  0 based column number
     */
    public za.co.enerweb.toolbox.poi.ss.Cell getCell(int cellnum) {
        Cell cell = poiRow.getCell(cellnum);
        if (cell == null) {
            cell = poiRow.createCell(cellnum);
            sheet.updateColumnLimits(cellnum);
        }
        return new za.co.enerweb.toolbox.poi.ss.Cell(this, cell);
    }

    public boolean hasCell(int cellnum) {
        return poiRow.getCell(cellnum) != null;
    }

    za.co.enerweb.toolbox.poi.ss.Sheet getTbSheet() {
        return sheet;
    }

    // vvv delegated methods

    public Cell createCell(int column) {
        return poiRow.createCell(column);
    }

    public Iterator<Cell> iterator() {
        return poiRow.iterator();
    }

    public Cell createCell(int column, int type) {
        return poiRow.createCell(column, type);
    }

    public void removeCell(Cell cell) {
        poiRow.removeCell(cell);
    }

    public void setRowNum(int rowNum) {
        poiRow.setRowNum(rowNum);
    }

    public int getRowNum() {
        return poiRow.getRowNum();
    }

    public Cell getCell(int cellnum, MissingCellPolicy policy) {
        return poiRow.getCell(cellnum, policy);
    }

    public short getFirstCellNum() {
        return poiRow.getFirstCellNum();
    }

    public short getLastCellNum() {
        return poiRow.getLastCellNum();
    }

    public int getPhysicalNumberOfCells() {
        return poiRow.getPhysicalNumberOfCells();
    }

    public void setHeight(short height) {
        poiRow.setHeight(height);
    }

    public void setZeroHeight(boolean zHeight) {
        poiRow.setZeroHeight(zHeight);
    }

    public boolean getZeroHeight() {
        return poiRow.getZeroHeight();
    }

    public void setHeightInPoints(float height) {
        poiRow.setHeightInPoints(height);
    }

    public short getHeight() {
        return poiRow.getHeight();
    }

    public float getHeightInPoints() {
        return poiRow.getHeightInPoints();
    }

    public Iterator<Cell> cellIterator() {
        return poiRow.cellIterator();
    }

    public Sheet getSheet() {
        return poiRow.getSheet();
    }

    public boolean isFormatted() {
        return poiRow.isFormatted();
    }

    public CellStyle getRowStyle() {
        return poiRow.getRowStyle();
    }

    public void setRowStyle(CellStyle style) {
        poiRow.setRowStyle(style);
    }
}
