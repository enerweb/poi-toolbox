package za.co.enerweb.toolbox.poi.ss;

import lombok.Getter;

import org.apache.poi.ss.usermodel.CellStyle;

public enum Border {
    NONE(CellStyle.BORDER_NONE),
    HAIR(CellStyle.BORDER_HAIR),
    THIN(CellStyle.BORDER_THIN),
    MEDIUM(CellStyle.BORDER_MEDIUM),
    THICK(CellStyle.BORDER_THICK),
    DOUBLE(CellStyle.BORDER_DOUBLE),
    DOTTED(CellStyle.BORDER_DOTTED),
    MEDIUM_DASHED(CellStyle.BORDER_MEDIUM_DASHED),
    // we can add the others but why?
    ;

    @Getter
    private short poiType;

    Border(short poiType) {
        this.poiType = poiType;
    }
}
