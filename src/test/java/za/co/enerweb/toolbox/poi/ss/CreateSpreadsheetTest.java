package za.co.enerweb.toolbox.poi.ss;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CreateSpreadsheetTest {

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    private String filename;
    private File file;
    private Spreadsheet spreadsheet;

    public CreateSpreadsheetTest(String filename) {
        this.filename = filename;
    }

    @Before
    public void setUp() throws IOException {
        file = tempFolder.newFile(filename);
        file.delete(); // make sure poi creates it!
        System.out.println("Testing file: " + file.getAbsolutePath());
        spreadsheet = Spreadsheet.forFile(file);
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {"test.xls"},
            {"test.xlsx"}
          });
    }

    @Test
    public void saveEmptyTest() throws IOException {
        assertFalse(file.exists());
        spreadsheet.save();
        assertTrue(file.exists());
    }

    @Test
    public void addSheetTest() throws IOException, InvalidFormatException {
        assertFalse(spreadsheet.hasSheet("testing123"));
        assertEquals(0, spreadsheet.getWorkbook().getNumberOfSheets());

        testSheetName("testing123", "testing123");
        testSheetName("", "empty");
        // some chars become blank
        testSheetName("~!@#$%^&*()_+{<>,./?-'\"\\",
                      "~!@#$%^& ()_+{<>,.  -'\" ");
        // if the name is too long it gets truncated
        testSheetName("1234567890123456789012345678901234567890",
                      "1234567890123456789012345678901");
        spreadsheet.save();

        spreadsheet = Spreadsheet.forFile(file).load();

        // check that we can look up the sheets by its original name
        assertTrue(spreadsheet.hasSheet("testing123"));
        assertTrue(spreadsheet.hasSheet(""));
        assertTrue(spreadsheet.hasSheet("~!@#$%^&*()_+{<>,./?-'\"\\"));
        assertTrue(spreadsheet.hasSheet(
            "1234567890123456789012345678901234567890"));

        // iterate over sheets, they will have the safe names
        Iterator<Sheet> iterator = spreadsheet.sheets().iterator();
        assertEquals("testing123", iterator.next().getName());
        assertEquals("empty", iterator.next().getName());
        assertEquals("~!@#$%^& ()_+{<>,.  -'\" ", iterator.next().getName());
        assertEquals("1234567890123456789012345678901",
            iterator.next().getName());

        // make sure the iterator can iterate to its end
        assertFalse(iterator.hasNext());
    }

    private void testSheetName(String nameIn, String expectedName) {
        assertEquals(expectedName, spreadsheet.getSheet(nameIn).getSheetName());
    }

    @Test
    public void addDataTest() throws IOException, InvalidFormatException {
        SheetWalker sw = spreadsheet.getSheetWalker("test-sheet");

        // vvv populate a column at a time

        sw.moveTo(0, 0).value("number").style(
            spreadsheet.createHeaderCellStyle().bottom(Border.THIN));
        sw.nextRow().value(123);

        sw.nextColumnTop().value("double").style(
            spreadsheet.createHeaderCellStyle().bottom(Border.MEDIUM));
        double doubleValue = 1234.567891234;
        sw.nextRow().value(doubleValue)
            .style(spreadsheet.createCellStyle().number());

        sw.nextColumnTop().value("formatted double").style(
            spreadsheet.createHeaderCellStyle().bottom(Border.THICK));
        sw.nextRow().value(doubleValue);

        sw.nextColumnTop(1, 0).value("text").style(
            spreadsheet.createHeaderCellStyle().thinBorder());
        sw.nextRow().value("abc").style(
            spreadsheet.createCellStyle().thinBorder());

        sw.nextColumnTop(2, 0).value("date").style(
            spreadsheet.createHeaderCellStyle().bottom(Border.MEDIUM_DASHED));
        Date now = new Date();
        sw.nextRow().value(now)
            .style(spreadsheet.createCellStyle().date());

        sw.nextColumnTop(1, 0).value("date-time").style(
            spreadsheet.createHeaderCellStyle().bottom(Border.DOUBLE));
        sw.nextRow().value(now)
            .style(spreadsheet.createCellStyle().dateTime());

        sw.getSheet().autoSizeColumns();
        spreadsheet.save();

        // vvv read some values to check that it worked

        spreadsheet = Spreadsheet.forFile(file).load();
        sw = spreadsheet.getSheetWalker("test-sheet");
        assertEquals("number", sw.getCell(0, 0).getValue());
        assertEquals("text", sw.getCell(1, 3).getValue());
        assertEquals(doubleValue, sw.getCell(1, 1).getValue());
        assertEquals(now, sw.getCell(3, 4).getDateCellValue());
    }

    @Test
    public void addNumberTable() throws IOException, InvalidFormatException {
        SheetWalker sw = spreadsheet.getSheetWalker("test-sheet");

        // vvv add like a separate table into the sheet, to show off the
        // relative nature of the api
        int startRow = 5; // where you want it to start
        int startColumn = 1;
        CellStyle heading = spreadsheet.createHeaderCellStyle()
            .thinBorder();

        // add the headings
        sw.moveTo(startRow, startColumn).value("index").style(heading);
        sw.mark();
        for (int j = 2; j <= 5; j++) {
            sw.nextRow().value("index / " + j).style(heading);
        }
        sw.moveToMark();

        CellStyle thinBorderNumber = spreadsheet.createCellStyle()
            .thinBorder().number("0.000"); // sets the number format
        for (int i = 0; i < 15; i++) {
            sw.nextColumn().value(i+1).style(heading);
            sw.mark();
            for (int j = 2; j <= 5; j++) {
                sw.nextRow().value((i+1.0)/j).style(thinBorderNumber);
            }
            sw.moveToMark();
        }
        sw.getSheet().autoSizeColumns();
        spreadsheet.save();

        spreadsheet = Spreadsheet.forFile(file).load();
        sw = spreadsheet.getSheetWalker("test-sheet");
        sw.moveTo(startRow, startColumn); // prepare for nextColumn()
        // add one column at a time
        for (int i = 0; i < 15; i++) {
            assertEquals(i+1.0, sw.nextColumn().getValue());
            for (int j = 2; j <= 5; j++) {
                assertEquals((i+1.0)/j, sw.getRelativeCell(j-1, 0).getValue());
            }
        }
    }
}
