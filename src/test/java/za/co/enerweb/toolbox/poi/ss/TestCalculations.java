package za.co.enerweb.toolbox.poi.ss;

import static org.junit.Assert.assertEquals;

import java.io.File;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

@Slf4j
public class TestCalculations {
    private static final String SHEET1 = "sheet 1";

    private static final String SHEET2 = "sheet 2";

    private static final double EPSILON = 0.00001;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    private static final String FILENAME = "test.xlsm";
    private File file;
    private Spreadsheet spreadsheet;

    private SheetWalker sw1;
    private SheetWalker sw2;

    @SneakyThrows
    @Before
    public void setUp() {
        file = tempFolder.newFile(FILENAME);
        file.delete(); // make sure poi creates it!
        log.info("Testing file: " + file.getAbsolutePath());
        spreadsheet = Spreadsheet.forFile(file);
        sw1 = spreadsheet.getSheetWalker(SHEET1);
        sw2 = spreadsheet.getSheetWalker(SHEET2);
    }

    @SneakyThrows
    @After
    public void tearDown() {
        spreadsheet.save();
    }

    @Test
    public void simpleCalculation() {
        sw1.getCell(0, 0).setCellValue(1);
        sw1.getCell(0, 1).setCellValue(2);
        Cell formulaCell = sw1.getCell(0, 2);
        formulaCell.setCellFormula("A1+B1");
        spreadsheet.evaluateAllFormulaCells();
        double value = formulaCell.getValue();
        assertEquals(3d, value, EPSILON);
    }

    @Test
    public void crossSheetCalculation() {
        sw1.getCell(0, 0).setCellValue(1);
        sw2.getCell(0, 1).setCellValue(2);
        Cell formulaCell = sw1.getCell(0, 2);
        formulaCell.setCellFormula("A1+'" + SHEET2 + "'!B1");
        // looks like this in libre office: =A1+$'sheet 2'.B1
        spreadsheet.evaluateAllFormulaCells();
        double value = formulaCell.getValue();
        assertEquals(3d, value, EPSILON);
    }
}
