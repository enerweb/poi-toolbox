package za.co.enerweb.toolbox.poi.ss;

import static org.junit.Assert.assertEquals;

import java.io.File;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

@Slf4j
public class TestCellValues {
    private static final String SHEET1 = "sheet 1";

    private static final double EPSILON = 0.00001;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    private static final String FILENAME = "test.xlsm";
    private File file;
    private Spreadsheet spreadsheet;

    private SheetWalker sw1;

    @SneakyThrows
    @Before
    public void setUp() {
        file = tempFolder.newFile(FILENAME);
        file.delete(); // make sure poi creates it!
        log.info("Testing file: " + file.getAbsolutePath());
        spreadsheet = Spreadsheet.forFile(file);
        sw1 = spreadsheet.getSheetWalker(SHEET1);
    }

    @SneakyThrows
    @After
    public void tearDown() {
        spreadsheet.save();
    }

    @Test
    public void cellValue() {
        Cell cell = sw1.getCell(0, 0);
        cell.setCellValue(1);
        assertEquals(1d, (Double) cell.getValue(), EPSILON);
        assertEquals(1d, (Double) cell.getDoubleValue(), EPSILON);
        assertEquals(1, (Integer) cell.getIntValue(), EPSILON);
        assertEquals(1L, (Long) cell.getLongValue(), EPSILON);
    }

    @Test
    public void formulaValue() {
        sw1.getCell(0, 0).setCellValue(6);
        sw1.getCell(0, 1).setCellValue(3);
        Cell formulaCell = sw1.getCell(0, 2);
        formulaCell.setCellFormula("A1/B1");
        spreadsheet.evaluateAllFormulaCells();
        assertEquals(6 / 3d, (Double) formulaCell.getValue(), EPSILON);
    }

    @Test(expected = IllegalStateException.class)
    public void formulaError() {
        sw1.getCell(0, 0).setCellValue(1);
        sw1.getCell(0, 1).setCellValue(0);
        Cell formulaCell = sw1.getCell(0, 2);
        formulaCell.setCellFormula("A1/B1");
        spreadsheet.evaluateAllFormulaCells();
        double value = formulaCell.getValue();
    }

}
