package za.co.enerweb.toolbox.poi.ss;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SpreadsheetOutputStreamTest {

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    private String filename;
    private File file;
    private Spreadsheet spreadsheet;

    public SpreadsheetOutputStreamTest(String filename) {
        this.filename = filename;
    }

    @Before
    public void setUp() throws IOException {
        file = tempFolder.newFile(filename);
        file.delete(); // make sure poi creates it!
        System.out.println("Testing file: " + file.getAbsolutePath());
        spreadsheet = Spreadsheet.forStream(file.getName(),
            new FileOutputStream(file));
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {"test.xls"},
            {"test.xlsx"}
          });
    }

    @Test
    public void saveEmptyTest() throws IOException {
        assertTrue(file.length() == 0);
        spreadsheet.save();
        assertTrue(file.exists());
        assertTrue(file.length() > 0);
        // XXX: we need to read the file to be able to test that it was
        // properly writtend
    }
}
